package main

import (
	"bufio"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"time"
)

/* path to files */
const (
	pathError = "/root/error.log"
	pathUsers = "/root/users.txt"
)

/* structure to save instance of "JSON's" value for function */
type Answer struct {
	Value string `json:"Value"`
}

/* function to handle error */
func handleError(err error) bool {

	/* handle error */
	if err != nil {

		var (
			fileErr *os.File
		)

		/* write into error logfile */
		fileErr, _ = os.OpenFile(pathError, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		_, _ = fileErr.WriteString(time.Now().String() + ":" + err.Error() + "\n")

		/* error exists */
		return true

	}

	/* error does not exist */
	return false

}

func addLogin(respWriter http.ResponseWriter, req *http.Request) {

	/* declaration of variables */
	var (
		params      map[string]string
		login       string
		fileUsers   *os.File
		err         error
		scanner     *bufio.Scanner
		line        string
		answer      Answer
	)

	/* default value */
	answer.Value = "yes"

	/* receive login */
	params = mux.Vars(req)
	login = params["login"]

	/* open file */
	fileUsers, err = os.OpenFile(pathUsers, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	handleError(err)

	/* create scanner */
	scanner = bufio.NewScanner(fileUsers)
	/* read file */
	for scanner.Scan() {
		line = scanner.Text()
		/* check login */
		if line == login {
			answer.Value = "no"
			break
		}
		if err != nil {
			break
		}
	}

	/* if login does not exist, then we will write new login */
	if answer.Value == "yes" {
		_, err = fileUsers.WriteString(login + "\n")
		handleError(err)
	}

	/* send value of function */
	respWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(respWriter).Encode(answer)
	if handleError(err) {
		return
	}

}

func isLoginFree(respWriter http.ResponseWriter, req *http.Request) {

	/* declaration of variables */
	var (
		params      map[string]string
		login       string
		fileUsers   *os.File
		err         error
		scanner     *bufio.Scanner
		line        string
		answer      Answer
	)

	/* default value */
	answer.Value = "yes"

	/* receive login */
	params = mux.Vars(req)
	login = params["login"]

	/* open file */
	fileUsers, err = os.Open(pathUsers)
	handleError(err)

	/* create reader */
	scanner = bufio.NewScanner(fileUsers)
	/* read file */
	for scanner.Scan() {
		line = scanner.Text()
		/* check login */
		if line == login {
			answer.Value = "no"
			break
		}
		if err != nil {
			break
		}
	}

	/* send value of function */
	respWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(respWriter).Encode(answer)
	if handleError(err) {
		return
	}

}

func main() {

	/* declaration of router */
	var (
		router *mux.Router
	)

	/* make handlers for function */
	router = mux.NewRouter()
	router.HandleFunc("/isLoginFree/{login}", isLoginFree).Methods("GET")
	router.HandleFunc("/addLogin/{login}", addLogin).Methods("GET")
	router.HandleFunc("/updateLogin/{login}&{nlogin}", addLogin).Methods("GET")

	/* launch REST-service */
	log.Fatal(http.ListenAndServe("0.0.0.0:8000", router))

}
